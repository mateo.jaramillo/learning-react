import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div>
        <h1>Welcome From Home Page!!</h1>
        <p>Hello world</p>
        {/* El link reemplaza al <a></a> puesto que no necesita recargar la página sino que inyecta el html */}
        <Link to="/contact"> 
            <button>Go to Contact</button>
        </Link>
    </div>
  )
}

export default Home