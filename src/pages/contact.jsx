import React from 'react'
import { Link } from 'react-router-dom'

const Contact = () => {
  return (
    <div>
        <h1>Contact's Page!!</h1>
        <Link to="/">
            <button>Go back Home!</button>
        </Link>
    </div>
  )
}

export default Contact