// import logo from './logo.svg';
import './App.css';
import Home from 'pages/home';
import Contact from 'pages/contact';
import {
  BrowserRouter as Router,
  // Switch as Routes,
  Routes as Switch,
  Route,
  // Switch
  // Link,
  // useRouteMatch,
  // useParams
} from "react-router-dom";
import Layout from 'layouts/Layout';


function App() {
  return (
    <div className="App">
      <Router>
        <Layout>
          <Switch>

            <Route path='/contact' element={<Contact />}/>
            <Route path='/' element={<Home />} />

          </Switch>
        </Layout>

        {/* Puedo poner cada layout dentro del switch pero indicando que la ruta es exact */}

        {/* <Switch>
          <Layout>

            <Route exact/>
            <Route exact/>

          </Layout>

          <Layout2>

            <Route exact/>
            <Route exact/>

          </Layout2>

        </Switch> */}

        {/* Tambien puedo crear un Switch diferente para cada layout pero dentro del mismo router */}

      </Router>
    </div>
  );
}

export default App;
